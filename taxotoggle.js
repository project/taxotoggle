
$(document).ready(function(){
  $("li:has(a[href*='taxonomy/term/'])").add("li[class*='taxonomy_term_']").draggable({
    helper: "clone"
  });
	
  $('.node').droppable({
    accept: 'li',
    activeClass: 'droppable-active',
    hoverClass: 'droppable-hover',
    drop: function(ev, ui) {
	  node = $(this);
	  nid = node.attr('id').split('-')[1];
      matches1 = ui.draggable.find('a').attr('href').match(/taxonomy\/term\/([0-9]+)/);
	  if (matches1) tid = matches1[1];
	  else {
		matches2 = ui.draggable.attr('class').match(/taxonomy_term_([0-9]+)/);
		if (matches2) tid = matches2[1];
		else return;
	  }
	  node.find('.terms').addClass('throbbing');
	  
      // Perform a request to the server
	  jQuery.ajax({
	    'url': '/node/' + nid + '/taxotoggle/' + tid,
		'type': 'POST',
		'cache': false,
		'data': { confirm: true, javascript: true },
		'dataType': 'html',
		'success': function(data) {
		  node.find('.terms ul').remove();
		  node.find('.terms').append(data);
		  node.find('.terms').removeClass('throbbing').find('li').draggable({helper:'clone'});
		},
		'error': function() {
		  node.find('.taxonomy').removeClass('throbbing');
		}
	  });
    }
  });
});
